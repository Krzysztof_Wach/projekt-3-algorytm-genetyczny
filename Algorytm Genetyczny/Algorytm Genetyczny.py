from random import randrange, choice, shuffle


class Populacja:
    def __init__(self, wielkosc = 100):
        self.pokolenie = []
        self.wielkosc = wielkosc

        while len(self.pokolenie) != self.wielkosc:
            self.pokolenie.append(Osobnik())
        print('pokolenie zostalo wygenerowane')

    def alogrytmGenetyczny(self, metoda_selekcji = 'losowo', metoda_krzyzowania = 'losowo', wartosc_mutacji = 10, cel=45):
        ocena_pokolenia = self.ocenaPokolenia(self.pokolenie) #wstepne okreslenie oceny pokolenia
        licznik_generacji = 1
        print('ocena pokolenia nr', licznik_generacji, ': ', ocena_pokolenia, '. Wartosc docelowa: ', cel)

        if wartosc_mutacji.isdigit() is True: #kontrola mutacji
            if wartosc_mutacji < 0:
                wartosc_mutacji = 0
            elif wartosc_mutacji > 100:
                wartosc_mutacji = 100
        else:
            wartosc_mutacji = 10

        if cel.isdigit() is True: #kontrola celu
            if cel < 11:
                cel = 11
            elif cel > 89:
                cel = 89
        else:
            cel = 45

        while ocena_pokolenia < cel:
            wynik_selekcji = self.selekcja(metoda_selekcji)
            nowe_pokolenie = self.krzyzowanie(wynik_selekcji, metoda_krzyzowania, wartosc_mutacji)
            ocena_pokolenia = self.ocenaPokolenia(nowe_pokolenie)
            self.pokolenie = nowe_pokolenie
            licznik_generacji+=1
            print('ocena pokolenia nr', licznik_generacji, ': ', ocena_pokolenia, '. Wartosc docelowa: ', cel)

    def ocenaPokolenia(self, pokolenie): #obliczanie sredniego wyniku pokolenia (max = 89, min = -89)
        ocena_pokolenia = 0
        for jednostka in pokolenie:
            ocena_pokolenia+= jednostka.ocena
        ocena_pokolenia= ocena_pokolenia/len(pokolenie)
        return ocena_pokolenia

    def selekcja(self, metoda_selekcji):
        metody = {'kolo ruletki' : self.koloRuletki, 'turniej' : self.turniej, 'ranking' : self.ranking}
        if metoda_selekcji in metody:
            return metody[metoda_selekcji]()
        else:
            return choice(list(metody.values()))()

    #selekcja 1
    def koloRuletki(self):
        zwyciezcy = []
        losowanie = []
        while len(losowanie) != self.wielkosc/5:
            losowanie.append(randrange(0, 100))

        #dopuszczamy tylko jednostki z pozytywna ocena
        suma_funkcji_oceny = 0
        przedzial = 0
        for jednostka in self.pokolenie:
            if jednostka.ocena >= 0:
                suma_funkcji_oceny+=jednostka.ocena

        for jednostka in self.pokolenie:
            if jednostka.ocena >= 0:
                przedzial+= (jednostka.ocena/suma_funkcji_oceny)*100
                for wylosowany in losowanie:
                    if przedzial >= wylosowany:
                        losowanie.remove(wylosowany)
                        zwyciezcy.append(jednostka)

        return zwyciezcy

    #selekcja 2
    def turniej(self):
        zwyciezcy = []

        for i in range(int(self.wielkosc/5)):
            zwyciezcy.append(self.drugaTura())

        return zwyciezcy

    def pierwszaTura(self):
        pierwsza_tura = []
        for i in range(int(self.wielkosc/20)):
            pierwsza_tura.append(choice(self.pokolenie))

        najwiekszy = pierwsza_tura[0]
        for i in pierwsza_tura:
            if i.ocena > najwiekszy.ocena:
                najwiekszy = i
        return najwiekszy

    def drugaTura(self):
        druga_tura = []
        for i in range(int(self.wielkosc/20)):
            druga_tura.append(self.pierwszaTura())

        najwiekszy = druga_tura[0]
        for i in druga_tura:
            if i.ocena > najwiekszy.ocena:
                najwiekszy = i
        return najwiekszy

    #selekcja 3
    def ranking(self):
        zwyciezcy = []
        pula = self.pokolenie

        #sortowanie
        for i in range(len(pula)-1):
            for j in range(len(pula)-1):
                if pula[j].ocena < pula[j+1].ocena:
                    hold = pula[j]
                    pula[j] = pula[j+1]
                    pula[j+1] = hold

        ilosc_zwyciezcow = self.wielkosc/5
        for i in range(int(ilosc_zwyciezcow)):
            zwyciezcy.append(pula[i])

        return zwyciezcy

    def krzyzowanie(self, wynik_selekcji, metoda_krzyzowania, wartosc_mutacji): #wynik selekcji = [20 Osobnicy]
        metody = {'jednopunktowe' : self.jednopunktowe, 'dwupunktowe' : self.dwupunktowe}
        if metoda_krzyzowania in metody:
            wybrana_metoda = metody[metoda_krzyzowania]
        else:
            wybrana_metoda = choice(list(metody.values()))

        pula = []
        nowe_pokolenie = []
        wskaznik_mutacji = False

        while len(nowe_pokolenie) != self.wielkosc:
            if len(pula) <= 1:
                for i in wynik_selekcji:
                    pula.append(i)
            else:
                #mutacja
                if randrange(0, 100) <= wartosc_mutacji:
                    wskaznik_mutacji = True

                rodzic1 = choice(pula)
                pula.remove(rodzic1)
                rodzic2 = choice(pula)
                pula.remove(rodzic2)
                wynik = wybrana_metoda(rodzic1, rodzic2, wskaznik_mutacji)
                nowe_pokolenie.append(wynik[0])
                nowe_pokolenie.append(wynik[1])

        return nowe_pokolenie


    def jednopunktowe(self, rodzic1, rodzic2, wskaznik_mutacji):
        #chromosom = [50genow]
        chromosom1 = []
        chromosom2 = []
        miejsce_przeciecia = randrange(1,49)

        chromosom1 = rodzic1.chromosom[0:miejsce_przeciecia] + rodzic2.chromosom[miejsce_przeciecia:len(rodzic2.chromosom)]
        chromosom2 = rodzic2.chromosom[0:miejsce_przeciecia] + rodzic1.chromosom[miejsce_przeciecia:len(rodzic1.chromosom)]

        if wskaznik_mutacji == True:
            miejsce_mutacji = randrange(0,50)
            mutacja = randrange(10,99)
            chromosom1[miejsce_mutacji] = mutacja
            chromosom2[miejsce_mutacji] = mutacja


        Dziecko1 = Osobnik(chromosom = chromosom1)
        Dziecko2 = Osobnik(chromosom = chromosom2)
        return [Dziecko1, Dziecko2]
        #wybierz dwie osoby z puli. skrzyzuj [wybierz losowo punkt przeciecia], przypisz wynik do nowego pokolenia

    def dwupunktowe(self, rodzic1, rodzic2, wskaznik_mutacji):
        # chromosom = [50genow]
        chromosom1 = []
        chromosom2 = []
        miejsce_przeciecia = [randrange(1, 49), randrange(1,49)]
        miejsce_przeciecia.sort()

        chromosom1 = rodzic1.chromosom[0:miejsce_przeciecia[0]] + rodzic2.chromosom[miejsce_przeciecia[0]:miejsce_przeciecia[1]] + rodzic1.chromosom[
                                                               miejsce_przeciecia[1]:len(rodzic1.chromosom)]
        chromosom2 = rodzic2.chromosom[0:miejsce_przeciecia[0]] + rodzic1.chromosom[miejsce_przeciecia[0]:miejsce_przeciecia[1]] + rodzic2.chromosom[
                                                               miejsce_przeciecia[1]:len(rodzic2.chromosom)]

        if wskaznik_mutacji == True:
            miejsce_mutacji = randrange(0, 50)
            mutacja = randrange(10, 99)
            chromosom1[miejsce_mutacji] = mutacja
            chromosom2[miejsce_mutacji] = mutacja

        Dziecko1 = Osobnik(chromosom=chromosom1)
        Dziecko2 = Osobnik(chromosom=chromosom2)
        return [Dziecko1, Dziecko2]
        # wybierz dwie osoby z puli. skrzyzuj [wybierz losowo punkt przeciecia], przypisz wynik do nowego pokolenia


class Osobnik:
    def __init__(self, chromosom='random'):
        self.chromosom = []
        if chromosom == 'random':
            while len(self.chromosom) != 50:
                self.chromosom.append(randrange(10, 99))
        else:
            self.chromosom = chromosom
        self.ocena = round(self.generujOcene(), 0)

    def generujOcene(self):
        parzyste = 0
        nieparzyste = 0
        for gen in range(len(self.chromosom)):
            if gen%2 == 0:
                parzyste+=self.chromosom[gen]
            else:
                nieparzyste += self.chromosom[gen]
        parzyste = parzyste/(len(self.chromosom)/2)
        nieparzyste = nieparzyste / (len(self.chromosom) / 2)
        return parzyste-nieparzyste

    #gen -> 50*[10-99] | chromosom(50 genow)
    #cel ->przarzyste geny, mają największą wartość ; nieparzyste geny, najmniejszą

if __name__ == "__main__":
    print('podaj wielkosc populacji (domyslnie 100)')
    wielkosc = input()
    if wielkosc.isdigit() == False:
        wielkosc = 100  # wielkosc
    else:
        wielkosc =int(wielkosc)
    pop = Populacja(wielkosc)

    print('podaj sposob selekcji kandydatow. Dostepne: kolo ruletki ; turniej ; ranking (domyslnie: losowo)')
    selekcja = input()

    print('podaj sposob krzyzowania kandydatow. Dostepne: jednopunktowe ; dwupunktowe (domyslnie: losowo)')
    krzyzowanie = input()

    print('podaj szanse na wystapienie mutacji (od 0 do 100) (domyslnie 10)')
    wartosc_mutacji = input()

    print('podaj cel (docelowa wartosc genow w populacji) (od 11 do 89) (domyslnie 45) \n',
          '(uwaga! wyznaczenie maksymalnej mozliwej wartosci moze znacznie wydluzyc prace programu)')
    cel = input()

    pop.alogrytmGenetyczny(selekcja, krzyzowanie, wartosc_mutacji, cel) #metoda selekcji, metoda krzyzowania, wartosc mutacji, cel
